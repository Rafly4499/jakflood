@extends("layouts.global")
@section("title") Detail Laporan Bantuan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>{{$helpreport->alamat}}</h1>
            </div><br><br>

        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">          
                <div class="card mx-auto ">
                    <div id="mapid" style="width: 1000px; height: 350px">
                        <script>
                            var mymap = L.map('mapid').setView([-7.118736, 112.416550], 13);
                            L.tileLayer(
                                'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                                        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                                    id: 'mapbox/streets-v11',
                                }).addTo(mymap);

                            L.marker([{{$helpreport->latitude}},{{$helpreport->longitude}}]).addTo(mymap)
                                .bindPopup('Tanggal Kejadian : {{$helpreport->tanggal}}, <br> Pelapor : {{$helpreport->user->name}}, <br> Alamat : {{$helpreport->alamat}}, <br> Jenis Kerusakan : {{$helpreport->jenis_kerusakan}}, <br> Jenis Bantuan : {{$helpreport->jenis_bantuan}}, <br> Telepon : {{$helpreport->telepon}}, <br> Keterangan : {{$helpreport->keterangan}}')
                                .openPopup();
                        </script>
                    </div>
                </div>
        </div>
    </div>
</section>
@endsection