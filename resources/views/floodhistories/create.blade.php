@extends("layouts.global")
@section("title") Tambah Data Riwayat Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10" action="{{route('floodhistories.store')}}"
            method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <div class="form-group">
                    <label for="tanggal">Tanggal Kejadian</label>
                    <input value="{{old('tanggal')}}"
                        class="form-control {{$errors->first('tanggal') ? "is-invalid" : ""}}"
                        placeholder="Masukkan tanggal" type="date" name="tanggal" id="tanggal" />
                    <div class="invalid-feedback">
                        {{$errors->first('tanggal')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="villages">Desa</label><br>
                    <select name="villages" class="form-control">
                        @foreach($village as $key =>$item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="kepala_keluarga">Jumlah KK</label>
                    <input value="{{old('kepala_keluarga')}}"
                        class="form-control {{$errors->first('kepala_keluarga') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah kepala keluarga terdampak banjir" type="number"
                        name="kepala_keluarga" id="kepala_keluarga" />
                    <div class="invalid-feedback">
                        {{$errors->first('kepala_keluarga')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="jiwa">Jumlah Jiwa</label>
                    <input value="{{old('jiwa')}}" class="form-control {{$errors->first('jiwa') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah korban jiwa terdampak banjir" type="number" name="jiwa"
                        id="jiwa" />
                    <div class="invalid-feedback">
                        {{$errors->first('jiwa')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="rumah">Jumlah Rumah</label>
                    <input value="{{old('rumah')}}" class="form-control {{$errors->first('rumah') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah rumah terdampak banjir" type="number" name="rumah" id="rumah" />
                    <div class="invalid-feedback">
                        {{$errors->first('rumah')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="sekolah">Jumlah Sekolah</label>
                    <input value="{{old('sekolah')}}"
                        class="form-control {{$errors->first('sekolah') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah sekolah terdampak banjir" type="number" name="sekolah"
                        id="sekolah" />
                    <div class="invalid-feedback">
                        {{$errors->first('sekolah')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="kantor_desa">Kantor Desa</label>
                    <input value="{{old('kantor_desa')}}"
                        class="form-control {{$errors->first('kantor_desa') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah kantor desa terdampak banjir" type="number" name="kantor_desa"
                        id="kantor_desa" />
                    <div class="invalid-feedback">
                        {{$errors->first('kantor_desa')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="sawah">Luas Sawah</label>
                    <input value="{{old('sawah')}}" class="form-control {{$errors->first('sawah') ? "is-invalid" : ""}}"
                        placeholder="Masukkan jumlah sawah terdampak banjir" type="text" name="sawah" id="sawah" />
                    <div class="invalid-feedback">
                        {{$errors->first('sawah')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="jalan">Tinggi genangan air di jalan</label>
                    <input value="{{old('jalan')}}" class="form-control {{$errors->first('jalan') ? "is-invalid" : ""}}"
                        placeholder="Masukkan tinggi genangan air di jalan" type="text" name="jalan" id="jalan" />
                    <div class="invalid-feedback">
                        {{$errors->first('jalan')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{old('latitude')}}" class="form-control {{$errors->first('latitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi latitude banjir" type="text" name="latitude" id="latitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('latitude')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{old('longitude')}}" class="form-control {{$errors->first('longitude') ? "is-invalid" : ""}}"
                        placeholder="Masukkan posisi longitude banjir" type="text" name="longitude" id="longitude" />
                    <div class="invalid-feedback">
                        {{$errors->first('longitude')}}
                    </div>    
                </div>
                <div class="form-group">
                    <label for="status">Status</label><br>
                    <select class="form-control" name="status" id="status">
                        <option value="BARU">BARU</option>
                        <option value="SELESAI">SELESAI</option>
                    </select>
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</section>
@endsection