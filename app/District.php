<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $timestamps = false;
    
    public function villages() {
        return $this->hasMany('App\VillageDistrict');
    }

    public function rivers() {
        return $this->hasMany('App\River');
    }
}
