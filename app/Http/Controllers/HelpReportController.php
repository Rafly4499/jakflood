<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HelpReportController extends Controller
{

    public function __construct(){
        $this->middleware(function($request, $next){
            if(Gate::allows('manage-operator')) return $next($request);
            abort(403, 'Anda tidak memiliki cukup hak akses');
            });
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $keyword = $request->get('keyword') ? $request->get('keyword') : '';
        
        if($status){
            $helpreports = \App\Helpreport::where('status', strtoupper($status))->paginate(10);
        } else {
            $helpreports = \App\Helpreport::paginate(10);
        }

        return view('helpreports.index', ['helpreports' => $helpreports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view('helpreports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_helpreport = new \App\Helpreport;
        $new_helpreport->tanggal = $request->get('tanggal');
        $new_helpreport->alamat = $request->get('alamat');
        $new_helpreport->jenis_kerusakan = $request->get('jenis_kerusakan');
        $new_helpreport->jenis_bantuan = $request->get('jenis_bantuan');
        $new_helpreport->telepon = $request->get('telepon');
        $new_helpreport->keterangan = $request->get('keterangan');
        
        $foto = $request->file('foto');
            if($foto){
                $foto_path = $foto->store('helpreport-fotos', 'public');
                $new_helpreport->foto = $foto_path;
        }
        $new_helpreport->latitude = $request->get('latitude');
        $new_helpreport->longitude = $request->get('longitude');
        $new_helpreport->status = $request->get('status');

        $new_helpreport->user_id = \Auth::user()->id;
        $new_helpreport->created_by = \Auth::user()->id;
        $new_helpreport->save();

        return redirect()->route('helpreports.index')->with('status', 'Data laporan bantuan telah berhasil dibuat');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $helpreport = \App\Helpreport::findOrFail($id);

        return view('helpreports.show', ['helpreport' => $helpreport]);
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $helpreport_to_edit = \App\Helpreport::findOrFail($id);
        
        return view('helpreports.edit', ['helpreport' => $helpreport_to_edit]);
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $helpreport = \App\Helpreport::findOrFail($id);

        $helpreport->tanggal = $request->get('tanggal');
        $helpreport->alamat = $request->get('alamat');
        $helpreport->jenis_kerusakan = $request->get('jenis_kerusakan');
        $helpreport->jenis_bantuan = $request->get('jenis_bantuan');
        $helpreport->telepon = $request->get('telepon');
        $helpreport->keterangan = $request->get('keterangan');

        $new_foto = $request->file('foto');

        if($new_foto){
            if($helpreport->foto && file_exists(storage_path('app/public/' . $helpreport->foto))){
            \Storage::delete('public/'. $helpreport->foto);
        }
        $new_foto_path = $new_foto->store('helpreport-fotos', 'public');
        
        $helpreport->foto = $new_foto_path;
        }

        $helpreport->latitude = $request->get('latitude');
        $helpreport->longitude = $request->get('longitude');
        $helpreport->status = $request->get('status');

        $helpreport->updated_by = \Auth::user()->id;

        $helpreport->save();

        return redirect()->route('helpreports.index', ['id'=>$helpreport->id])->with('status', 'Laporan bantuan berhasil diperbarui');
   
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $helpreport = \App\Helpreport::findOrFail($id);

        $helpreport->delete();

        return redirect()->route('helpreports.index')->with('status', 'Laporan telah diarsipkan');
  
    }

    public function trash(){
        $deleted_helpreport = \App\Helpreport::onlyTrashed()->paginate(10);
        
        return view('helpreports.trash', ['helpreports' => $deleted_helpreport]);
    }

    public function restore($id){
        $helpreport = \App\Helpreport::withTrashed()->findOrFail($id);
        
        if($helpreport->trashed()){
            $helpreport->restore();
        } else {
            return redirect()->route('helpreports.index')->with('status', 'Laporan tidak ada di data arsip');
        }

        return redirect()->route('helpreports.index')->with('status', 'Laporan telah berhasil direstore');
    }

    public function deletePermanent($id){
        $helpreport = \App\Helpreport::withTrashed()->findOrFail($id);
        
        if(!$helpreport->trashed()){
            return redirect()->route('helpreports.index')->with('status', 'Laporan aktif, Tidak bisa hapus permanen!');
        } else {
            $helpreport->forceDelete();
            return redirect()->route('helpreports.index')->with('status', 'Laporan telah berhasil dihapus permanen');
        }
    }
}
