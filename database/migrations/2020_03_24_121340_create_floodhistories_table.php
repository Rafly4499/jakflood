<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloodhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floodhistories', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("village_id")->unsigned();
            $table->date("tanggal");
            $table->integer("kepala_keluarga");
            $table->integer("jiwa");
            $table->integer("rumah");
            $table->integer("sekolah");
            $table->integer("kantor_desa");
            $table->string("sawah");
            $table->string("jalan");
            $table->string("latitude")->nullable();;
            $table->string("longitude")->nullable();;
            $table->enum("status", ["BARU","SELESAI"]);
            $table->integer("created_by")->nullable();
            $table->integer("updated_by")->nullable();
            $table->integer("deleted_by")->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('village_id')->references('id')->on('villages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('floodhistories', function(Blueprint $table){
            $table->dropForeign('floodhistories_village_id_foreign');
            });

        Schema::dropIfExists('floodhistories');
    }
}
