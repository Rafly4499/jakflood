<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FloodHistory;
use App\Village;

class FloodHistoriesController extends Controller
{
    public function action(){
  
        $data = FloodHistory::where('status','BARU')->with('village')->get();

        $params = [
            'code' => 200,
            'message' => 'Get Data Banjir Success!',
            'databanjir' => $data,
      
        ];
        return response()->json($params);

    }
}
