@extends("layouts.global")
@section("title") Data Arsip Riwayat Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Riwayat Banjir</h1>
            </div><br><br>
            <div class="col-sm-10">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('floodhistories.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('floodhistories.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Desa</b></th>
                                    <th><b>Kecamatan</b></th>
                                    <th><b>Jumlah KK</b></th>
                                    <th><b>Jumlah Jiwa</b></th>
                                    <th><b>Jumlah Rumah</b></th>
                                    <th><b>Jumlah Sekolah</b></th>
                                    <th><b>Jumlah Kantor Desa</b></th>
                                    <th><b>Luas Sawah</b></th>
                                    <th><b>Tinggi Genangan di Jalan</b></th>
                                    <th><b>Latitude</b></th>
                                    <th><b>Longitude</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($floodhistories as $floodhistory)
                                <tr>
                                    <td>{{$floodhistory->tanggal}}</td>
                                    <td>{{$floodhistory->village['name']}}</td>
                                    <td>{{$floodhistory->village->subdistrict['name']}}</td>
                                    <td>{{$floodhistory->kepala_keluarga}}</td>
                                    <td>{{$floodhistory->jiwa}}</td>
                                    <td>{{$floodhistory->rumah}}</td>
                                    <td>{{$floodhistory->sekolah}}</td>
                                    <td>{{$floodhistory->kantor_desa}}</td>
                                    <td>{{$floodhistory->sawah}}</td>
                                    <td>{{$floodhistory->jalan}}</td>
                                    <td>{{$floodhistory->latitude}}</td>
                                    <td>{{$floodhistory->longitude}}</td>
                                    <td>
                                        @if($floodhistory->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$floodhistory->status}}</span>
                                        @elseif($floodhistory->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$floodhistory->status}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('floodhistories.restore', ['id' => $floodhistory->id])}}"
                                            class="btn btn-success btn-sm">Restore</a>
                                        <form class="d-inline"
                                            action="{{route('floodhistories.delete-permanent', ['id' => $floodhistory->id])}}"
                                            method="POST"
                                            onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini secara permanen?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$floodhistories->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection