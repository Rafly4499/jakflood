<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\HelpReport;
use App\User;


class HelpReportController extends Controller
{
    public function action (Request $request)
    {   
        $this->validate($request, [
            'tanggal' => 'required',
            'alamat' => 'required',
            'jenis_kerusakan' => 'required',
            'jenis_bantuan' => 'required',
            'telepon' => 'required',
            'keterangan' => 'required',
            'foto' => 'required|image',
        ]);

        $helpreport = HelpReport::create([
            'user_id' => auth()->user()->id, 
            'tanggal' => $request->tanggal,
            'alamat' => $request->alamat,
            'jenis_kerusakan' => $request->jenis_kerusakan,
            'jenis_bantuan' => $request->jenis_bantuan,
            'telepon' => $request->telepon,
            'keterangan' => $request->keterangan,
            'foto' => $request->file('foto')->store('helpreport-fotos'),
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'status' => "BARU",
        ]);


        return response()->json([
            "data" => $helpreport,
            "message" => "Laporan berhasil ditambahkan"
        ], 201);
    }
}
