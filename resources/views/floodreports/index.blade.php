@extends("layouts.global")
@section("title") Data Laporan Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Laporan Banjir</h1>
            </div><br><br>
            <div class="col-sm-10">
            @if (Auth::user()->role==2)
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('floodreports.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'baru' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'baru'])}}">Data Baru</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'proses' ? 'active' : '' }}"
                            href="{{route('floodreports.index', ['status' => 'proses'])}}">Data Diproses</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'selesai' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'selesai'])}}">Data Terverifikasi</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'cancel' ?'active' : '' }}"
                            href="{{route('floodreports.index', ['status' =>'cancel'])}}">Data Dicancel</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::path() == 'floodreports/trash' ? 'active' :''}}"
                            href="{{route('floodreports.trash')}}">Data Arsip</a></li>
                </ol>
            @endif
            </div>
            <div class="col-sm-2 text-right">
            @if (Auth::user()->role==3)
                <a href="{{route('floodreports.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            @endif
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Pelapor</b></th>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Foto</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($floodreports as $floodreport)
                                <tr>
                                    <td>{{$floodreport->user['username']}}</td>
                                    <td>{{$floodreport->tanggal}}</td>
                                    <td> @if($floodreport->foto)
                                            <img src="{{asset('storage/' . $floodreport->foto)}}" width="100px" />
                                        @endif
                                    </td>
                                    <td>
                                        @if($floodreport->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$floodreport->status}}</span>
                                        @elseif($floodreport->status == "PROSES")
                                        <span class="badge bg-info text-light">{{$floodreport->status}}
                                        </span>
                                        @elseif($floodreport->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$floodreport->status}}</span>
                                        @elseif($floodreport->status == "CANCEL")
                                        <span class="badge bg-dark text-light">{{$floodreport->status}}
                                        </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                            href="{{route('floodreports.edit',['floodreport'=>$floodreport->id])}}">Edit</a>
                                        <a class="btn btn-success btn-sm"
                                            href="{{route('floodreports.show',['floodreport'=>$floodreport->id])}}">Info</a>
                                        <form
                                            onsubmit="return confirm('Apakah Anda yakin ingin mengarsipkan data ini ?')"
                                            class="d-inline"
                                            action="{{route('floodreports.destroy', ['floodreport' => $floodreport->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Arsip" class="btn btn-warning btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$floodreports->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection