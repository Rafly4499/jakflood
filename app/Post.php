<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    protected $table = 'posts';

    use SoftDeletes;

    public function village(){
        return $this->belongsTo('App\Village');
    }
    
}
