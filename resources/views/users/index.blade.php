@extends("layouts.global")
@section("title") Users list @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data User</h1>
            </div><br><br>
            <div class="col-sm-12 text-right">
                <a href="{{route('users.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        @if($user->role == 1)
                                        Admin
                                        @endif

                                        @if($user->role == 2)
                                        Operator
                                        @endif

                                        @if($user->role == 3)
                                        Lapor
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-info text-white btn-sm"
                                            href="{{route('users.edit',['user'=>$user->id])}}">Edit</a>
                                        <form onsubmit="return confirm('Delete this user permanently?')"
                                            class="d-inline" action="{{route('users.destroy', ['user' => $user->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                        </form>
                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$users->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection