<?php

use Illuminate\Database\Seeder;

class LaporSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lapor = new \App\User;
        $lapor->username = "lapor";
        $lapor->email = "lapor@gmail.com";
        $lapor->role = "3";
        $lapor->password = \Hash::make("lapor");
        
        $lapor->save();
        
        $this->command->info("User lapor berhasil diinsert");
    }
}
