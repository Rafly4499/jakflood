@extends("layouts.global")
@section("title") Data Laporan Bantuan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Laporan Bantuan</h1>
            </div><br><br>
            <div class="col-sm-10">
            @if (Auth::user()->role==2)
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('helpreports.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'baru' ?'active' : '' }}"
                            href="{{route('helpreports.index', ['status' =>'baru'])}}">Data Baru</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'proses' ? 'active' : '' }}"
                            href="{{route('helpreports.index', ['status' => 'proses'])}}">Data Diproses</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'selesai' ?'active' : '' }}"
                            href="{{route('helpreports.index', ['status' =>'selesai'])}}">Data Terverifikasi</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'cancel' ?'active' : '' }}"
                            href="{{route('helpreports.index', ['status' =>'cancel'])}}">Data Dicancel</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::path() == 'helpreports/trash' ? 'active' :''}}"
                            href="{{route('helpreports.trash')}}">Data Arsip</a></li>
                </ol>
            @endif
            </div>
            <div class="col-sm-2 text-right">
            @if (Auth::user()->role==3)
                <a href="{{route('helpreports.create')}}" class="btn btn-primary btn-sm">Tambah</a>
            @endif
            </div>
        </div>
        @if(session('status'))
        <div class="alert">
            {{session('status')}}
        </div>
        @endif
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Pelapor</b></th>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Foto</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($helpreports as $helpreport)
                                <tr>
                                    <td>{{$helpreport->user['username']}}</td>
                                    <td>{{$helpreport->tanggal}}</td>
                                    <td> @if($helpreport->foto)
                                            <img src="{{asset('storage/' . $helpreport->foto)}}" width="100px" />
                                        @endif
                                    </td>
                                    <td>
                                        @if($helpreport->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$helpreport->status}}</span>
                                        @elseif($helpreport->status == "PROSES")
                                        <span class="badge bg-info text-light">{{$helpreport->status}}
                                        </span>
                                        @elseif($helpreport->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$helpreport->status}}</span>
                                        @elseif($helpreport->status == "CANCEL")
                                        <span class="badge bg-dark text-light">{{$helpreport->status}}
                                        </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                            href="{{route('helpreports.edit',['helpreport'=>$helpreport->id])}}">Edit</a>
                                        <a class="btn btn-success btn-sm"
                                            href="{{route('helpreports.show',['helpreport'=>$helpreport->id])}}">Info</a>
                                        <form
                                            onsubmit="return confirm('Apakah Anda yakin ingin mengarsipkan data ini ?')"
                                            class="d-inline"
                                            action="{{route('helpreports.destroy', ['helpreport' => $helpreport->id ])}}"
                                            method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Arsip" class="btn btn-warning btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$helpreports->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection