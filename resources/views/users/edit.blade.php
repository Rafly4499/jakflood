@extends("layouts.global")
@section("title") Edit User @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('users.update', ['user'=>$user->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input value="{{$user->username}}" type="text" disabled class="form-control" id="username"
                        name="name" placeholder="Masukkan Username Anda">
                </div>
                <div class="form-group">
                    <label for="">Roles</label>
                    <select class="form-control" name="role" id="role">
                        <option value="1">Admin</option>
                        <option value="2">Operator</option>
                        <option value="3">Lapor</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{$user->email}}" type="email" class="form-control" id="email" name="email"
                        placeholder="Masukkan Email Anda">
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
                <!-- <input class="btn btn-primary" type="submit" value="Save" /> -->
            </div>
        </form>
    </div>
</section>
@endsection