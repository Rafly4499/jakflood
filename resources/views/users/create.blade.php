@extends("layouts.global")
@section("title") Create New User @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10" action="{{route('users.store')}}"
            method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <div class="form-group">
                    <label for="username">Username</label>
                     <input value="{{old('username')}}"
                        class="form-control {{$errors->first('username') ? "is-invalid" : ""}}" placeholder="username"
                        type="text" name="username" id="username" />
                    <div class="invalid-feedback">
                        {{$errors->first('username')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Roles</label>
                    <select class="form-control {{$errors->first('role') ? "is-invalid" : ""}}" name="role" id="role">
                        <option value="1">Admin</option>
                        <option value="2">Operator</option>
                        <option value="3">Lapor</option>
                    </select>
                    <div class="invalid-feedback">
                        {{$errors->first('role')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{old('email')}}" class="form-control {{$errors->first('email') ? "is-invalid" : ""}}"
                        placeholder="Masukkan Email Anda" type="text" name="email" id="email" />
                    <div class="invalid-feedback">
                        {{$errors->first('email')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control {{$errors->first('password')? "is-invalid": ""}}" placeholder="password" type="password" name="password" id="password" />
                    <div class="invalid-feedback">
                        {{$errors->first('password')}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Konfirmasi Password</label>
                    <input class="form-control {{$errors->first('password_confirmation')? "is-invalid" : ""}}" placeholder="password confirmation" type="password" name="password_confirmation"
                        id="password_confirmation" />
                    <div class="invalid-feedback">
                        {{$errors->first('password_confirmation')}}
                    </div>
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</section>
@endsection