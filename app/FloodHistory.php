<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FloodHistory extends Model
{
    protected $table = 'floodhistories';

    use SoftDeletes;

    public function village(){
        return $this->belongsTo('App\Village');
    }
    
}
