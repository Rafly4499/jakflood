@extends("layouts.global")
@section("title") Data Arsip Laporan Bantuan @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row m-2 ">
            <div class="col-lg-12 text-center">
                <h1>Data Laporan Bantuan</h1>
            </div><br><br>
            <div class="col-sm-10">
            <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="{{route('helpreports.index')}}">Data Aktif</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'baru' ?'active' : '' }}" href="{{route('helpreports.index', ['status' =>'baru'])}}">Data Baru</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'proses' ? 'active' : '' }}" href="{{route('helpreports.index', ['status' => 'proses'])}}">Data Diproses</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'selesai' ?'active' : '' }}" href="{{route('helpreports.index', ['status' =>'selesai'])}}">Data Terverifikasi</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::get('status') == 'cancel' ?'active' : '' }}" href="{{route('helpreports.index', ['status' =>'cancel'])}}">Data Dicancel</a></li>
                    <li class="breadcrumb-item"><a class="{{Request::path() == 'helpreports/trash' ? 'active' :''}}" href="{{route('helpreports.trash')}}">Data Arsip</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">

                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th><b>Nama Pelapor</b></th>
                                    <th><b>Tanggal</b></th>
                                    <th><b>Alamat</b></th>
                                    <th><b>Jenis Kerusakan</b></th>
                                    <th><b>Jenis Bantuan</b></th>
                                    <th><b>Telepon</b></th>
                                    <th><b>Keterangan</b></th>
                                    <th><b>Foto</b></th>
                                    <th><b>Latitude</b></th>
                                    <th><b>Longitude</b></th>
                                    <th><b>Status</b></th>
                                    <th><b>Opsi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($helpreports as $helpreport)
                                <tr>
                                    <td>{{$helpreport->user['username']}}</td>
                                    <td>{{$helpreport->tanggal}}</td>
                                    <td>{{$helpreport->alamat}}</td>
                                    <td>{{$helpreport->jenis_kerusakan}}</td>
                                    <td>{{$helpreport->jenis_bantuan}}</td>
                                    <td>{{$helpreport->telepon}}</td>
                                    <td>{{$helpreport->keterangan}}</td>
                                    <td>
                                        @if($helpreport->foto)
                                        <img src="{{asset('storage/' . $helpreport->foto)}}" width="50px" />
                                        @endif
                                    </td>
                                    <td>{{$helpreport->latitude}}</td>
                                    <td>{{$helpreport->longitude}}</td>
                                      <td>
                                        @if($helpreport->status == "BARU")
                                        <span class="badge bg-warning text-light">{{$helpreport->status}}</span>
                                        @elseif($helpreport->status == "PROSES")
                                        <span class="badge bg-info text-light">{{$helpreport->status}}
                                        </span>
                                        @elseif($helpreport->status == "SELESAI")
                                        <span class="badge bg-success text-light">{{$helpreport->status}}</span>
                                        @elseif($helpreport->status == "CANCEL")
                                        <span class="badge bg-dark text-light">{{$helpreport->status}}
                                        </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('helpreports.restore', ['id' => $helpreport->id])}}"
                                            class="btn btn-success btn-sm">Restore</a>
                                        <form class="d-inline"
                                            action="{{route('helpreports.delete-permanent', ['id' => $helpreport->id])}}"
                                            method="POST"
                                            onsubmit="return confirm('Apakah Anda yakin ingin menghapus data ini secara permanen?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE" />
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="10">
                                        {{$helpreports->appends(Request::all())->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection