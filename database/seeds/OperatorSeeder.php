<?php

use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operator = new \App\User;
        $operator->username = "operator";
        $operator->email = "operator@gmail.com";
        $operator->role = "2";
        $operator->password = \Hash::make("operator");
        
        $operator->save();
        
        $this->command->info("User Operator berhasil diinsert");
    }
}
