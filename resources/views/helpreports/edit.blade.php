@extends("layouts.global")
@section("title") Edit Data Laporan Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10"
            action="{{route('helpreports.update', ['helpreport'=>$helpreport->id])}}" method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <div class="form-group">
                    <label for="tanggal">Tanggal Laporan</label>
                    <input value="{{$helpreport->tanggal}}" type="date" class="form-control" id="tanggal" name="tanggal"
                        placeholder="Masukkan tanggal">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat Laporan</label>
                    <input value="{{$helpreport->alamat}}" type="text" class="form-control" id="alamat" name="alamat"
                        placeholder="Masukkan Alamat">
                </div>
                <div class="form-group">
                    <label for="jenis_kerusakan">Jenis Kerusakan</label>
                    <input value="{{$helpreport->jenis_kerusakan}}" type="text" class="form-control" id="jenis_kerusakan" name="jenis_kerusakan"
                        placeholder="Masukkan jenis kerusakan">
                </div>
                <div class="form-group">
                    <label for="jenis_bantuan">Alamat Laporan</label>
                    <input value="{{$helpreport->jenis_bantuan}}" type="text" class="form-control" id="jenis_bantuan" name="jenis_bantuan"
                        placeholder="Masukkan jenis bantuan">
                </div>
                <div class="form-group">
                    <label for="telepon">Alamat Laporan</label>
                    <input value="{{$helpreport->telepon}}" type="text" class="form-control" id="telepon" name="telepon"
                        placeholder="Masukkan nomor telepon">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input value="{{$helpreport->keterangan}}" type="text" class="form-control" id="keterangan" name="keterangan"
                        placeholder="Masukkan keterangan">
                </div>
                <label for="foto">Foto</label><br>
                <small class="text-muted">Foto Awal</small><br>
                @if($helpreport->foto)
                <img src="{{asset('storage/' . $helpreport->foto)}}" width="50px" />
                @endif
                <br><br>
                <input type="file" class="form-control" name="foto">
                <small class="text-muted">Kosongkan jika tidak ingin mengubah
                    Foto</small>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input value="{{$helpreport->latitude}}" type="text" class="form-control" id="latitude" name="latitude"
                        placeholder="Masukkan posisi latitude Desa">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input value="{{$helpreport->longitude}}" type="text" class="form-control" id="longitude"
                        name="longitude" placeholder="Masukkan posisi longitude Desa">
                </div>
                <div class="form-group">
                <label for="status">Status</label><br>
                <select class="form-control" name="status" id="status">
                    <option {{$helpreport->status == "BARU" ? "selected" : ""}} value="BARU">BARU</option>
                    <option {{$helpreport->status == "PROSES" ? "selected" : ""}} value="PROSES">PROSES</option>
                    <option {{$helpreport->status == "SELESAI" ? "selected" : ""}} value="SELESAI">SELESAI</option>
                    <option {{$helpreport->status == "CANCEL" ? "selected" : ""}} value="CANCEL">CANCEL</option>
                </select>
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</section>
@endsection