@extends("layouts.global")
@section("title") Tambah Data Laporan Banjir @endsection
@section("content")
<section class="content-header">
    <div class="container-fluid">
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <form enctype="multipart/form-data" class="bg-white shadow-sm p-10" action="{{route('floodreports.store')}}"
            method="POST">
            <div class="card-body table-responsive p-10">
                @csrf
                <div class="form-group">
                    <label for="tanggal">Tanggal Laporan</label>
                    <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan tanggal">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat Laporan </label>
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat">
                </div>
                <div class="form-group">
                    <label for="tinggi_genangan">Tinggi Genangan</label>
                    <input type="text" class="form-control" id="tinggi_genangan" name="tinggi_genangan"
                        placeholder="Masukkan tinggi genangan banjir">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
                </div>
                <div class="form-group">
                    <label for="foto">Foto</label>
                    <input type="file" class="form-control" id="foto" name="foto" placeholder="Foto">
                </div>
                <div class="form-group">
                    <label for="latitude">Latitude</label>
                    <input type="text" class="form-control" id="latitude" name="latitude"
                        placeholder="Masukkan posisi latitude Laporan Banjir">
                </div>
                <div class="form-group">
                    <label for="longitude">Longitude</label>
                    <input type="text" class="form-control" id="longitude" name="longitude"
                        placeholder="Masukkan posisi longitude Laporan Banjir">
                </div>
                <div class="form-group">
                <label for="status">Status</label><br>
                <select class="form-control" name="status" id="status">
                    <option value="BARU">BARU</option>
                    <option value="PROSES">PROSES</option>
                    <option value="SELESAI">SELESAI</option>
                    <option value="CANCEL">CANCEL</option>
                </select>
                </div>
                <button type="submit" value="Save" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</section>
@endsection